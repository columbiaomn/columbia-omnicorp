Columbia Omni is one of the largest office and art supply companies in New York City. Located in midtown Manhattan, we have over 20,000 items in-stock. We carry a large range of products including pantone, lights, breakroom, janitorial and furniture as well as traditional office and art supplies.

Address: 14 West 33rd St, New York, NY 10001, USA

Phone: 212-279-6161

